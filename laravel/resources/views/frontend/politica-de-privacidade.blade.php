@extends('frontend.layout.template')

@section('content')

<main class="politica-de-privacidade">

    <section class="conteudo center">
        <h1 class="titulo">POLÍTICA DE PRIVACIDADE</h1>
        <article class="texto">{!! $politica->texto !!}</article>
    </section>

</main>

@endsection