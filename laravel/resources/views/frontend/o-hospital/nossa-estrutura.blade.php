@extends('frontend.layout.template')

@section('content')

<main class="o-hospital">

    <section class="conteudo center">
        @include('frontend.o-hospital._submenu')

        <article class="textos">
            <h2 class="titulo">Nossa Estrutura</h2>
            <div class="texto">{!! $hospital->nossa_estrutura !!}</div>
        </article>
    </section>

    @if(count($destaques) > 0)
    <section class="estrutura-destaques">
        @foreach($destaques as $destaque)
        <article class="destaque" id="{{ $destaque->id }}">
            <div class="center">
                @if(count($destaquesImagens) > 0)
                <div class="imagens">
                    @foreach($destaquesImagens->where('destaque_id', $destaque->id)->take(1) as $imagem)
                    <a href="{{ asset('assets/img/estrutura/destaques/big/'.$imagem->imagem) }}" class="link-img-destaque-capa fancybox" rel="estrutura_destaques_imagens">
                        <img src="{{ asset('assets/img/estrutura/destaques/'.$imagem->imagem) }}" class="img-destaque-capa" alt="">
                    </a>
                    @endforeach
                    <div class="variacoes">
                        @foreach($destaquesImagens->where('destaque_id', $destaque->id) as $imagem)
                        <a href="{{ asset('assets/img/estrutura/destaques/'.$imagem->imagem) }}" class="link-variacao" urlFancybox="{{ asset('assets/img/estrutura/destaques/big/'.$imagem->imagem) }}">
                            <img src="{{ asset('assets/img/estrutura/destaques/small/'.$imagem->imagem) }}" alt="" class="variacao">
                        </a>
                        @endforeach
                    </div>
                </div>
                @endif
                <div class="dados">
                    <div class="cx-titulo">
                        <p class="titulo">{{ $destaque->titulo }}</p>
                    </div>
                    <div class="texto">{!! $destaque->texto !!}</div>
                    @if($destaque->video)
                    <iframe class="video" src="{{ 'https://www.youtube.com/embed/'.$destaque->video }}"></iframe>
                    @endif
                </div>
            </div>
        </article>
        @endforeach
    </section>
    @endif

    <section class="estrutura-imagens center">
        <h2 class="titulo">CONFIRA MAIS IMAGENS DO HOSPITAL CENTRAL LESTE</h2>
        <article class="categorias">
            @foreach($categorias as $categoria)
            <a href="{{ route('o-hospital.getImagensCategoria', $categoria->id) }}" class="link-filtro-categoria">{{ $categoria->titulo }}</a>
            @endforeach
        </article>
        <article class="imagens">
            @foreach($imagensFirst as $imagem)
            <a href="{{ asset('assets/img/estrutura/imagens/big/'.$imagem->imagem) }}" class="link-imagem fancybox" rel="estrutura_imagens">
                <img src="{{ asset('assets/img/estrutura/imagens/small/'.$imagem->imagem) }}" class="img-estrutura" alt="">
            </a>
            @endforeach
        </article>
    </section>

</main>

@endsection