@extends('frontend.layout.template')

@section('content')

<main class="o-hospital">

    <section class="conteudo center">
        @include('frontend.o-hospital._submenu')

        <article class="textos">
            <h2 class="titulo">Qualidade e Segurança</h2>
            <div class="texto">{!! $hospital->qualidade_e_segurança !!}</div>
        </article>
    </section>

</main>

@endsection