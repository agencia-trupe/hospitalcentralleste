<article class="submenu">
    <img src="{{ asset('assets/img/layout/iconeArea-hospital.svg') }}" class="img-hospital">
    <div class="itens">
        <h1 class="titulo">O HOSPITAL</h1>
        <a href="{{ route('o-hospital', 'quem-somos') }}" class="link-submenu @if(url()->current() == route('o-hospital', 'quem-somos')) active @endif"><span>•</span> quem somos
            <img src="{{ asset('assets/img/layout/setinha-reta-hover.svg') }}" class="img-setinha">
        </a>
        <a href="{{ route('o-hospital', 'nossa-historia') }}" class="link-submenu @if(url()->current() == route('o-hospital', 'nossa-historia')) active @endif"><span>•</span> nossa história
            <img src="{{ asset('assets/img/layout/setinha-reta-hover.svg') }}" class="img-setinha">
        </a>
        <a href="{{ route('o-hospital', 'nossa-estrutura') }}" class="link-submenu @if(url()->current() == route('o-hospital', 'nossa-estrutura')) active @endif"><span>•</span> nossa estrutura
            <img src="{{ asset('assets/img/layout/setinha-reta-hover.svg') }}" class="img-setinha">
        </a>
        <a href="{{ route('o-hospital', 'qualidade-e-seguranca') }}" class="link-submenu @if(url()->current() == route('o-hospital', 'qualidade-e-seguranca')) active @endif"><span>•</span> qualidade e segurança
            <img src="{{ asset('assets/img/layout/setinha-reta-hover.svg') }}" class="img-setinha">
        </a>
    </div>
</article>