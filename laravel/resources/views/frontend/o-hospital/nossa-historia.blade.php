@extends('frontend.layout.template')

@section('content')

<main class="o-hospital">

    <section class="conteudo center">
        @include('frontend.o-hospital._submenu')

        <article class="textos">
            <h2 class="titulo">Nossa História</h2>
            <div class="texto">{!! $hospital->nossa_historia !!}</div>
        </article>
    </section>

    <section class="historico">
        <div class="center">
            <h4 class="titulo">HISTÓRICO DE CRESCIMENTO</h4>

            @if(count($historicoFundacao) > 0)
            <article class="fundacao">
                <h6 class="fase">FUNDAÇÃO</h6>
                <hr class="linha-historico">
                @foreach($historicoFundacao as $historico)
                <div class="item-historico">
                    <a href="{{ asset('assets/img/historico/big/'.$historico->imagem) }}" class="link-img-historico fancybox" rel="historico">
                        <img src="{{ asset('assets/img/historico/'.$historico->imagem) }}" class="img-historico" alt="">
                    </a>
                    <p class="ano">{{ $historico->ano }}</p>
                    <div class="bolinha"></div>
                    <p class="frase">{{ $historico->frase }}</p>
                </div>
                @endforeach
            </article>
            @endif

            @if(count($historicoExpansao) > 0)
            <article class="expansao">
                <h6 class="fase">EXPANSÃO</h6>
                @foreach($historicoExpansao as $historico)
                <div class="item-historico">
                    <a href="{{ asset('assets/img/historico/big/'.$historico->imagem) }}" class="link-img-historico fancybox" rel="historico">
                        <img src="{{ asset('assets/img/historico/'.$historico->imagem) }}" class="img-historico" alt="">
                    </a>
                    <p class="ano">{{ $historico->ano }}</p>
                    <div class="bolinha"></div>
                    <p class="frase">{{ $historico->frase }}</p>
                </div>
                @endforeach
            </article>
            @endif

            @if(count($historicoConsolidacao) > 0)
            <article class="consolidacao">
                <h6 class="fase">CONSOLIDAÇÃO</h6>
                @foreach($historicoConsolidacao as $historico)
                <div class="item-historico">
                    <a href="{{ asset('assets/img/historico/big/'.$historico->imagem) }}" class="link-img-historico fancybox" rel="historico">
                        <img src="{{ asset('assets/img/historico/'.$historico->imagem) }}" class="img-historico" alt="">
                    </a>
                    <p class="ano">{{ $historico->ano }}</p>
                    <div class="bolinha"></div>
                    <p class="frase">{{ $historico->frase }}</p>
                </div>
                @endforeach
            </article>
            @endif
        </div>
    </section>


</main>

@endsection