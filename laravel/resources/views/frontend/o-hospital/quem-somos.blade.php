@extends('frontend.layout.template')

@section('content')

<main class="o-hospital">

    <section class="conteudo center">
        @include('frontend.o-hospital._submenu')

        <article class="textos">
            <h2 class="titulo">Quem somos</h2>
            <div class="texto">{!! $hospital->quem_somos !!}</div>
        </article>
    </section>

    @if(count($quemSomos) > 0)
    <section class="quem-somos-imagens center">
        @foreach($quemSomos as $imagem)
        <a href="{{ asset('assets/img/quem-somos/big/'.$imagem->imagem) }}" class="link-img-quem-somos fancybox" rel="quem-somos">
            <img src="{{ asset('assets/img/quem-somos/'.$imagem->imagem) }}" class="img-quem-somos" alt="">
        </a>
        @endforeach
    </section>
    @endif

    <section class="missao-visao-valores center">
        <article class="missao">
            <img src="{{ asset('assets/img/layout/icone-missao.svg') }}" class="img-icone">
            <div class="textos">
                <h3 class="titulo">MISSÃO</h3>
                <div class="texto">{!! $hospital->missao !!}</div>
            </div>
        </article>

        <article class="visao">
            <img src="{{ asset('assets/img/layout/icone-visao.svg') }}" class="img-icone">
            <div class="textos">
                <h3 class="titulo">VISÃO</h3>
                <div class="texto">{!! $hospital->visao !!}</div>
            </div>
        </article>

        <article class="valores">
            <img src="{{ asset('assets/img/layout/icone-valores.svg') }}" class="img-icone">
            <div class="textos">
                <h3 class="titulo">VALORES</h3>
                <div class="texto">{!! $hospital->valores !!}</div>
            </div>
        </article>
    </section>

</main>

@endsection