@extends('frontend.layout.template')

@section('content')

<main class="servicos">

    <section class="conteudo center">
        <article class="submenu">
            <img src="{{ asset('assets/img/layout/iconeArea-servicos.svg') }}" class="img-servicos">
            <div class="itens">
                <h1 class="titulo">SERVIÇOS</h1>
                @foreach($servicos as $serv)
                <a href="{{ route('servicos', $serv->slug) }}" class="link-submenu @if(url()->current() == route('servicos', $serv->slug)) active @endif"><span>•</span> {{ $serv->titulo }}
                    <img src="{{ asset('assets/img/layout/setinha-reta-hover.svg') }}" class="img-setinha">
                </a>
                @endforeach
            </div>
        </article>

        <article class="textos">
            <h2 class="titulo">{{ $servico->titulo }}</h2>
            <div class="texto">{!! $servico->texto !!}</div>
        </article>
    </section>

    @if(count($destaques) > 0)
    <section class="destaques center masonry-grid">
        @foreach($destaques as $destaque)
        <article class="destaque">
            <a href="{{ asset('assets/img/servicos/destaques/big/'.$destaque->imagem) }}" class="link-img-destaque fancybox" rel="servicos_destaques">
                <img src="{{ asset('assets/img/servicos/destaques/'.$destaque->imagem) }}" class="img-destaque" alt="">
            </a>
            <div class="cx-titulo">
                <p class="titulo">{{ $destaque->titulo }}</p>
            </div>
            <div class="texto">{!! $destaque->texto !!}</div>
        </article>
        @endforeach
    </section>
    @endif

    @if($localizacao != null)
    <section class="localizacao center">
        <h1 class="titulo-localizacao">{{ $servico->titulo_localizacao }}</h1>
        <article class="informacoes">
            <div class="telefone">
                <img src="{{ asset('assets/img/layout/ico-telefone.svg') }}" alt="Telefone" class="img-telefone">
                <p class="titulo">TELEFONE</p>
            </div>
            @php
            $telefone = str_replace(" ", "", $contato->telefone);
            @endphp
            <a href="tel:{{ $telefone }}" class="link-telefone">{{ $contato->telefone }}</a>
            <hr class="linha-localizacao">
            <div class="endereco">
                <img src="{{ asset('assets/img/layout/ico-endereco.svg') }}" alt="Endereço" class="img-endereco">
                <p class="titulo">ENDEREÇO</p>
            </div>
            <p class="local">{{ $localizacao->endereco }}</p>
            <hr class="linha-localizacao">
            <div class="atendimento">
                <img src="{{ asset('assets/img/layout/ico-horario.svg') }}" alt="Horário" class="img-horario">
                <p class="titulo">HORÁRIOS DE ATENDIMENTO</p>
            </div>
            <p class="horario">Atendimento presencial 24 horas</p>
        </article>
        <article class="mapa">
            {!! $localizacao->google_maps !!}
        </article>
    </section>
    @endif

</main>

@endsection