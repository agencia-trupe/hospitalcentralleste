@extends('frontend.layout.template')

@section('content')

<main class="localizacao">

    <section class="conteudo center">
        <img src="{{ asset('assets/img/layout/iconeArea-localizacao.svg') }}" class="img-localizacao">
        <div class="textos">
            <h1 class="titulo">LOCALIZAÇÃO</h1>
            <div class="texto-localizacao">{!! $contato->texto_localizacao !!}</div>
        </div>
    </section>

    <section class="localizacoes center">
        @foreach($localizacoes as $local)
        <article class="local" id="localizacao-{{$local->id}}">
            <div class="mapa">{!! $local->google_maps !!}</div>
            <div class="infos-local">
                <div class="info-titulo">
                    <img src="{{ asset('assets/img/layout/ico-endereco-branco.svg') }}" alt="Endereço" class="img-endereco">
                    <p class="titulo">{{ $local->titulo }}</p>
                </div>
                <p class="info-endereco">{{ $local->endereco }}</p>
            </div>
        </article>
        @endforeach
    </section>

</main>

@endsection