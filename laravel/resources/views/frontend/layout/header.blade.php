<header>

    <nav class="topo">
        @include('frontend.layout.nav')
    </nav>

    <button id="mobile-toggle" type="button" role="button" class="@if(Tools::routeIs('home')) btn-home @endif">
        <span class="lines"></span>
    </button>

    <div class="down">
        @if(Tools::routeIs('home'))
        <a href="{{ route('home') }}" class="link-home">
            <img src="{{ asset('assets/img/layout/marca-hospital-leste.svg') }}" alt="{{ $config->title }}" class="img-logo">
        </a>
        @else
        <a href="{{ route('home') }}" class="link-home-internas">
            <img src="{{ asset('assets/img/layout/marca-hospital-leste-internas.svg') }}" alt="{{ $config->title }}" class="img-logo">
        </a>
        <nav class="servicos">
            @include('frontend.layout.nav-servicos')
        </nav>
        @endif
    </div>

</header>