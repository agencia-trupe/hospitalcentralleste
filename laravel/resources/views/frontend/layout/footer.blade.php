<footer>

    <article class="informacoes center">
        <a href="{{ route('home') }}" class="link-home">
            <img src="{{ asset('assets/img/layout/marca-hospital-leste-internas.svg') }}" alt="{{ $config->title }}" class="img-logo">
        </a>
        <div class="contatos">
            <div class="telefone">
                <img src="{{ asset('assets/img/layout/ico-telefone.svg') }}" alt="Telefone" class="img-telefone">
                <p class="titulo">TELEFONE</p>
            </div>
            @php
            $telefone = str_replace(" ", "", $contato->telefone);
            @endphp
            <a href="tel:{{ $telefone }}" class="link-telefone">{{ $contato->telefone }}</a>
            <a href="{{ $contato->instagram }}" target="_blank" class="instagram">@hospitalcentralleste</a>
        </div>
        <div class="enderecos">
            <div class="endereco">
                <img src="{{ asset('assets/img/layout/ico-endereco.svg') }}" alt="Endereço" class="img-endereco">
                <p class="titulo">ENDEREÇOS</p>
            </div>
            @foreach($localizacoes as $local)
            <div class="localizacao">
                <p class="titulo">[{{ $local->titulo }}]</p>
                <p class="local">{{ $local->endereco }}</p>
            </div>
            @endforeach
        </div>
    </article>

    <article class="menus center">
        <div class="servicos-header">
            @foreach($servicosHeader as $servico)
            <a href="{{ route('servicos', $servico->slug) }}" class="link-footer @if(url()->current() == route('servicos', $servico->slug)) active @endif">{{ $servico->titulo_header }}</a>
            @endforeach
        </div>
        <div class="o-hospital">
            <p class="titulo">O HOSPITAL</p>
            <a href="{{ route('o-hospital', 'quem-somos') }}" class="sublink-footer @if(url()->current() == route('o-hospital', 'quem-somos')) active @endif">quem somos</a>
            <a href="{{ route('o-hospital', 'nossa-historia') }}" class="sublink-footer @if(url()->current() == route('o-hospital', 'nossa-historia')) active @endif">nossa história</a>
            <a href="{{ route('o-hospital', 'nossa-estrutura') }}" class="sublink-footer @if(url()->current() == route('o-hospital', 'nossa-estrutura')) active @endif">nossa estrutura</a>
            <a href="{{ route('o-hospital', 'qualidade-e-seguranca') }}" class="sublink-footer @if(url()->current() == route('o-hospital', 'qualidade-e-seguranca')) active @endif">qualidade e segurança</a>
        </div>
        <div class="servicos-todos">
            <p class="titulo">SERVIÇOS</p>
            @foreach($servicos as $servico)
            <a href="{{ route('servicos', $servico->slug) }}" class="sublink-footer @if(url()->current() == route('servicos', $servico->slug)) active @endif">{{ $servico->titulo }}</a>
            @endforeach
        </div>
        <div class="links-geral">
            <a href="{{ route('novidades') }}" class="link-footer @if(Tools::routeIs('novidades*')) active @endif">NOVIDADES</a>
            <a href="{{ route('localizacao') }}" class="link-footer @if(Tools::routeIs('localizacao*')) active @endif">LOCALIZAÇÃO</a>
            <a href="{{ route('contato') }}" class="link-footer @if(Tools::routeIs('contato*')) active @endif">CONTATO</a>
        </div>
    </article>

    <article class="copyright">
        <a href="{{ route('politica-de-privacidade') }}" class="link-politica @if(Tools::routeIs('politica-de-privacidade')) active @endif">[POLÍTICA DE PRIVACIDADE]</a>
        <p class="dados"> © {{ date('Y') }} {{ config('app.name') }}</p>
        <span>•</span>
        <p class="direitos">Todos os direitos reservados</p>
        <span>|</span>
        <div class="links">
            <a href="https://www.trupe.net" target="_blank" class="link-trupe">Criação de sites: </a>
            <a href="https://www.trupe.net" target="_blank" class="link-trupe"> Trupe Agência Criativa</a>
        </div>
    </article>

</footer>