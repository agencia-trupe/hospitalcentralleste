<div class="center">

    <a href="{{ route('home') }}" class="link-nav @if(Tools::routeIs('home')) active @endif">HOME</a>
    <a href="{{ route('o-hospital', 'quem-somos') }}" class="link-nav @if(Tools::routeIs('o-hospital*')) active @endif" id="navSubHospital" submenu="submenu-hospital">O HOSPITAL</a>
    <a href="{{ route('servicos', $servicoFirst->slug) }}" class="link-nav @if(Tools::routeIs('servicos*')) active @endif" id="navSubServicos" submenu="submenu-servicos">SERVIÇOS</a>
    <a href="{{ route('novidades') }}" class="link-nav @if(Tools::routeIs('novidades*')) active @endif">NOVIDADES</a>
    <a href="{{ route('localizacao') }}" class="link-nav @if(Tools::routeIs('localizacao*')) active @endif">LOCALIZAÇÃO</a>
    <a href="{{ route('contato') }}" class="link-nav @if(Tools::routeIs('contato*')) active @endif">CONTATO</a>
    <a href="{{ $contato->instagram }}" target="_blank" class="instagram">
        <img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="Instagram" class="img-instagram">
    </a>

    <!-- submenu O HOSPITAL -->
    <div class="submenu-nav submenu-hospital" style="display: none;">
        <a href="{{ route('o-hospital', 'quem-somos') }}" class="link-submenu-nav @if(url()->current() == route('o-hospital', 'quem-somos')) active @endif">quem somos</a>
        <a href="{{ route('o-hospital', 'nossa-historia') }}" class="link-submenu-nav @if(url()->current() == route('o-hospital', 'nossa-historia')) active @endif">nossa história</a>
        <a href="{{ route('o-hospital', 'nossa-estrutura') }}" class="link-submenu-nav @if(url()->current() == route('o-hospital', 'nossa-estrutura')) active @endif">nossa estrutura</a>
        <a href="{{ route('o-hospital', 'qualidade-e-seguranca') }}" class="link-submenu-nav @if(url()->current() == route('o-hospital', 'qualidade-e-seguranca')) active @endif">qualidade e segurança</a>
    </div>

    <!-- submenu SERVIÇOS -->
    <div class="submenu-nav submenu-servicos" style="display: none;">
        @foreach($servicos as $servico)
        <a href="{{ route('servicos', $servico->slug) }}" class="link-submenu-nav @if(url()->current() == route('servicos', $servico->slug)) active @endif">{{ $servico->titulo }}</a>
        @endforeach
    </div>

</div>