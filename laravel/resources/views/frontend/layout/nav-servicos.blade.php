<div class="itens">

    <div class="chanfrado"></div>

    @foreach($servicosHeader as $servico)
    <a href="{{ route('servicos', $servico->slug) }}" class="link-nav-servicos @if(url()->current() == route('servicos', $servico->slug)) active @endif">{{ $servico->titulo_header }}</a>
    @endforeach

</div>