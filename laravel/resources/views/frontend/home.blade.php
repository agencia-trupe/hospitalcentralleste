@extends('frontend.layout.template')

@section('content')

<main class="home">

    <section class="bg-inicial">
        <div class="bg-imagem">
            <article class="banners center">
                @foreach($banners as $banner)
                <div class="banner">
                    <div class="textos">
                        <div class="titulo">{!! $banner->titulo !!}</div>
                        <div class="frase">{!! $banner->frase !!}</div>
                    </div>
                    <div class="imagem">
                        <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" class="img-banner">
                    </div>
                </div>
                @endforeach
            </article>

            <article class="servicos-home center">
                @foreach($servicosHome as $servico)
                <a href="{{ route('servicos', $servico->slug) }}" class="link-servico">
                    <img src="{{ asset('assets/img/servicos/'.$servico->capa_home) }}" class="img-servico">
                    <p class="titulo">{{ $servico->titulo_home }}</p>
                </a>
                @endforeach
            </article>
        </div>

        <article class="localizacoes">
            <div class="center">
                <div class="left">
                    <h1 class="titulo">{{ $home->titulo_acessos }}</h1>
                    <p class="frase">{{ $home->frase_acessos }}</p>
                </div>
                <div class="right">
                    @foreach($localizacoes as $local)
                    <div class="localizacao">
                        <img src="{{ asset('assets/img/localizacoes/'.$local->capa_home) }}" class="img-local">
                        <div class="dados">
                            <div class="principais-acessos">{!! $local->principais_acessos !!}</div>
                            <p class="endereco">{{ $local->endereco }}</p>
                        </div>
                        <a href="{{ route('localizacao', '#localizacao-'.$local->id) }}" class="link-mapa">
                            VER MAPA
                            <img src="{{ asset('assets/img/layout/setinha-reta.svg') }}" class="img-setinha">
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </article>
    </section>

    <section class="bg-chamadas">
        <article class="textos center">
            <h1 class="titulo">{{ $home->titulo_chamadas }}</h1>
            <h4 class="frase">{{ $home->frase_chamadas }}</h4>
            <div class="texto">{!! $home->texto_chamadas !!}</div>
        </article>

        @if(count($chamadas) > 0)
        <article class="chamadas center">
            @foreach($chamadas as $chamada)
            <div class="chamada">
                <img src="{{ asset('assets/img/chamadas/'.$chamada->imagem) }}" class="img-chamada">
                <p class="titulo">{{ $chamada->titulo }}</p>
                <a href="{{ $chamada->link }}" class="saiba-mais">
                    SAIBA MAIS
                    <div class="img-setinha"></div>
                </a>
            </div>
            @endforeach
        </article>
        @endif
    </section>

    @if(count($maisServicos) > 0)
    <section class="bg-mais-servicos">
        <h1 class="titulo">MAIS SERVIÇOS</h1>
        <div class="mais-servicos center">
            @foreach($maisServicos as $item)
            <a href="{{ $item->link }}" class="link-item">{{ $item->titulo }}</a>
            @endforeach
        </div>
    </section>
    @endif

    <section class="bg-novidades">
        <h1 class="titulo">{{ $home->titulo_novidades }}</h1>
        <p class="frase">{{ $home->frase_novidades }}</p>
        <div class="novidades center" id="instafeed">

        </div>
    </section>

</main>

@endsection