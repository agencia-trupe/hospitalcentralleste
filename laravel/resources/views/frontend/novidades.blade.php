@extends('frontend.layout.template')

@section('content')

<main class="novidades">

    <section class="titulo-pagina center">
        <img src="{{ asset('assets/img/layout/iconeArea-novidades.svg') }}" class="img-novidades">
        <h1 class="titulo">NOVIDADES</h1>
    </section>


</main>

@endsection