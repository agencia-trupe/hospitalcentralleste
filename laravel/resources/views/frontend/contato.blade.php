@extends('frontend.layout.template')

@section('content')

<main class="contato">

    <section class="conteudo center">

        <article class="infos">
            <div class="topo">
                <img src="{{ asset('assets/img/layout/iconeArea-contato.svg') }}" class="img-contato">
                <h1 class="titulo">CONTATO</h1>
            </div>
            <div class="texto-contatos">{!! $contato->texto_contatos !!}</div>
        </article>

        <article class="form-contato">
            <form action="{{ route('contato.post') }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="telefone" class="input-telefone" value="{{ old('telefone') }}">
                <select name="assunto" class="assunto">
                    <option value="" selected>ASSUNTO (selecione...)</option>
                    @foreach($assuntos as $key => $assunto)
                    <option value="{{ $key }}">{{ $assunto }}</option>
                    @endforeach
                </select>
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                <button type="submit" class="btn-enviar">ENVIAR
                    <img src="{{ asset('assets/img/layout/ico-enviar.svg') }}" alt="" class="img-enviar">
                </button>

                @if($errors->any())
                <div class="flash flash-erro">
                    @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                    @endforeach
                </div>
                @endif

                @if(session('enviado'))
                <div class="flash flash-sucesso">
                    <p>Mensagem enviada com sucesso!</p>
                </div>
                @endif
            </form>
        </article>

    </section>

</main>

@endsection