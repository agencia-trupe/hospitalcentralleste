@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<a href="{{ route('painel.servicos.index') }}" title="Voltar para Serviços" class="btn btn-secondary mb-3 col-2">
    &larr; Voltar para Serviços</a>

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0"><small>Serviço: {{ $servico->titulo }} |</small> Destaques</h2>


    <a href="{{ route('painel.servicos.destaques.create', $servico->id) }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Destaque
    </a>
</legend>


@if(!count($destaques))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="servicos_destaques">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Imagem</th>
                <th scope="col">Título</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($destaques as $destaque)
            <tr id="{{ $destaque->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>
                    <img src="{{ asset('assets/img/servicos/destaques/'.$destaque->imagem) }}" style="width: auto; max-width:100px;" alt="">
                </td>
                <td>{{ $destaque->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.servicos.destaques.destroy', $servico->id, $destaque->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.servicos.destaques.edit', [$servico->id, $destaque->id] ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection