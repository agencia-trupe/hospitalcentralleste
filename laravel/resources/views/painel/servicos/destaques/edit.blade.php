@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>Serviço: {{ $servico->titulo }} |</small> Editar Destaque</h2>
</legend>

{!! Form::model($destaque, [
'route' => ['painel.servicos.destaques.update', $servico->id, $destaque->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.servicos.destaques.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection