@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control editor-basic']) !!}
</div>

<hr>

<div class="mb-3 col-12">
    @if($submitText == 'Alterar')
    <div class="checkbox m-0 checkbox-header @if($servico->header == 1) checked @endif">
        @else
        <div class="checkbox m-0 checkbox-header">
            @endif
            <label style="font-weight:bold">
                {!! Form::hidden('header', 0) !!}
                {!! Form::checkbox('header') !!}
                Menu/Header
            </label>
        </div>
    </div>

    <div class="mb-3 col-12 col-md-12 conteudo-header">
        {!! Form::label('titulo_header', 'Título Menu/Header') !!}
        {!! Form::text('titulo_header', null, ['class' => 'form-control input-text']) !!}
    </div>

    <hr>

    <div class="mb-3 col-12">
        @if($submitText == 'Alterar')
        <div class="checkbox m-0 checkbox-home @if($servico->home == 1) checked @endif">
            @else
            <div class="checkbox m-0 checkbox-home">
                @endif
                <label style="font-weight:bold">
                    {!! Form::hidden('home', 0) !!}
                    {!! Form::checkbox('home') !!}
                    Home
                </label>
            </div>
        </div>

        <div class="mb-3 col-12 col-md-12 conteudo-home">
            {!! Form::label('capa_home', 'Capa Home') !!}
            @if($submitText == 'Alterar')
            @if($servico->capa_home)
            <img src="{{ url('assets/img/servicos/'.$servico->capa_home) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            @endif
            {!! Form::file('capa_home', ['class' => 'form-control']) !!}
        </div>

        <div class="mb-3 col-12 col-md-12 conteudo-home">
            {!! Form::label('titulo_home', 'Título Home') !!}
            {!! Form::text('titulo_home', null, ['class' => 'form-control input-text']) !!}
        </div>

        <hr>

        <div class="mb-3 col-12">
            @if($submitText == 'Alterar')
            <div class="checkbox m-0 checkbox-localizacao @if($servico->localizacao == 1) checked @endif">
                @else
                <div class="checkbox m-0 checkbox-localizacao">
                    @endif
                    <label style="font-weight:bold">
                        {!! Form::hidden('localizacao', 0) !!}
                        {!! Form::checkbox('localizacao') !!}
                        Localização
                    </label>
                </div>
            </div>

            <div class="mb-3 col-12 col-md-12 conteudo-localizacao">
                {!! Form::label('titulo_localizacao', 'Título Localização') !!}
                {!! Form::text('titulo_localizacao', null, ['class' => 'form-control input-text']) !!}
            </div>

            <div class="mb-3 col-12 conteudo-localizacao">
                {!! Form::label('localizacao_id', 'Localizações') !!}
                {!! Form::select('localizacao_id', $localizacoes , old('localizacao_id'), ['class' => 'form-control select-tipos']) !!}
            </div>

            <div class="d-flex align-items-center mt-4">
                {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

                <a href="{{ route('painel.servicos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
            </div>