@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>MAIS SERVIÇOS |</small> Editar Item</h2>
</legend>

{!! Form::model($servico, [
'route' => ['painel.mais-servicos.update', $servico->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.home.mais-servicos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection