@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>MAIS SERVIÇOS |</small> Adicionar Item</h2>
</legend>

{!! Form::open(['route' => 'painel.mais-servicos.store', 'files' => true]) !!}

@include('painel.home.mais-servicos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection