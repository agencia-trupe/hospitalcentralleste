@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CHAMADAS |</small> Adicionar Chamada</h2>
</legend>

{!! Form::open(['route' => 'painel.chamadas.store', 'files' => true]) !!}

@include('painel.home.chamadas.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection