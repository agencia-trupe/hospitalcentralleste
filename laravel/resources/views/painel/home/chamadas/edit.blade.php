@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CHAMADAS |</small> Editar Chamada</h2>
</legend>

{!! Form::model($chamada, [
'route' => ['painel.chamadas.update', $chamada->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.home.chamadas.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection