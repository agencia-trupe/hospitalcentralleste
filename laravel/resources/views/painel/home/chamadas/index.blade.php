@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">CHAMADAS</h2>

    <a href="{{ route('painel.chamadas.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Chamada
    </a>
</legend>


@if(!count($chamadas))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="chamadas">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Imagem</th>
                <th scope="col">Título</th>
                <th scope="col">Link</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($chamadas as $chamada)
            <tr id="{{ $chamada->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>
                    <img src="{{ asset('assets/img/chamadas/'.$chamada->imagem) }}" style="width: auto; max-width:100px;" alt="">
                </td>
                <td>{{ $chamada->titulo }}</td>
                <td>{{ $chamada->link }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.chamadas.destroy', $chamada->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.chamadas.edit', $chamada->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection