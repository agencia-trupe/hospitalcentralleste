@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('titulo_acessos', 'ACESSOS - Título') !!}
    {!! Form::text('titulo_acessos', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('frase_acessos', 'ACESSOS - Frase') !!}
    {!! Form::text('frase_acessos', null, ['class' => 'form-control input-text']) !!}
</div>

<hr>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('titulo_chamadas', 'CHAMADAS - Título') !!}
    {!! Form::text('titulo_chamadas', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('frase_chamadas', 'CHAMADAS - Frase') !!}
    {!! Form::text('frase_chamadas', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_chamadas', 'CHAMADAS - Texto') !!}
    {!! Form::textarea('texto_chamadas', null, ['class' => 'form-control editor-basic']) !!}
</div>

<hr>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('titulo_novidades', 'NOVIDADES - Título') !!}
    {!! Form::text('titulo_novidades', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('frase_novidades', 'NOVIDADES - Frase') !!}
    {!! Form::text('frase_novidades', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}
</div>