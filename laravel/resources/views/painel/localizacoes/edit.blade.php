@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>LOCALIZAÇÕES |</small> Editar Localização</h2>
</legend>

{!! Form::model($localizacao, [
'route' => ['painel.localizacoes.update', $localizacao->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.localizacoes.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection