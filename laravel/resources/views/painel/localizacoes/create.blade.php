@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>LOCALIZAÇÕES |</small> Adicionar Localização</h2>
</legend>

{!! Form::open(['route' => 'painel.localizacoes.store', 'files' => true]) !!}

@include('painel.localizacoes.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection