@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('titulo', 'Título (Localização)') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('titulo_rodape', 'Título (Rodapé)') !!}
    {!! Form::text('titulo_rodape', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('endereco', 'Endereço Completo') !!}
    {!! Form::text('endereco', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('capa_home', 'Capa Home') !!}
    @if($submitText == 'Alterar')
    @if($localizacao->capa_home)
    <img src="{{ url('assets/img/localizacoes/'.$localizacao->capa_home) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('capa_home', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('principais_acessos', 'Principais Acessos (Home)') !!}
    {!! Form::textarea('principais_acessos', null, ['class' => 'form-control editor-basic']) !!}
    <p class="obs"><strong>OBS:</strong> listar principais acessos usando: <img src="{{ url('assets/img/layout/bullet-list.png') }}" alt="" style="max-width: 30px;"></p>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.localizacoes.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>