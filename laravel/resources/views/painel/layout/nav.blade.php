<ul class="nav navbar-nav">

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['painel.home*', 'painel.banners*', 'painel.chamadas*', 'painel.mais-servicos*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuHome" data-bs-toggle="dropdown" aria-expanded="false">
            Home <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuHome">
            <li>
                <a href="{{ route('painel.home.index') }}" class="dropdown-item @if(Tools::routeIs('painel.home*')) active @endif">Home | Textos</a>
            </li>
            <li>
                <a href="{{ route('painel.banners.index') }}" class="dropdown-item @if(Tools::routeIs('painel.banners*')) active @endif">Banners</a>
            </li>
            <li>
                <a href="{{ route('painel.chamadas.index') }}" class="dropdown-item @if(Tools::routeIs('painel.chamadas*')) active @endif">Chamadas</a>
            </li>
            <li>
                <a href="{{ route('painel.mais-servicos.index') }}" class="dropdown-item @if(Tools::routeIs('painel.mais-servicos*')) active @endif">Mais Serviços</a>
            </li>
        </ul>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['painel.hospital*', 'painel.quem-somos*', 'painel.historico*', 'painel.estrutura.destaques*', 'painel.estrutura.categorias*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuHome" data-bs-toggle="dropdown" aria-expanded="false">
            O Hospital <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuHome">
            <li>
                <a href="{{ route('painel.hospital.index') }}" class="dropdown-item @if(Tools::routeIs('painel.hospital*')) active @endif">O Hospital | Textos</a>
            </li>
            <li>
                <a href="{{ route('painel.quem-somos.imagens.index') }}" class="dropdown-item @if(Tools::routeIs('painel.quem-somos*')) active @endif">Quem Somos | Imagens</a>
            </li>
            <li>
                <a href="{{ route('painel.historico.index') }}" class="dropdown-item @if(Tools::routeIs('painel.historico*')) active @endif">Nossa História | Histórico</a>
            </li>
            <li>
                <a href="{{ route('painel.estrutura.destaques.index') }}" class="dropdown-item @if(Tools::routeIs('painel.estrutura.destaques*')) active @endif">Nossa Estrutura | Destaques</a>
            </li>
            <li>
                <a href="{{ route('painel.estrutura.categorias.index') }}" class="dropdown-item @if(Tools::routeIs('painel.estrutura.categorias*')) active @endif">Nossa Estrutura | Imagens</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ route('painel.servicos.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.servicos*')) active @endif">Serviços</a>
    </li>

    <li>
        <a href="{{ route('painel.localizacoes.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.localizacoes*')) active @endif">Localização</a>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['painel.contatos*', 'painel.assuntos*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuContatos" data-bs-toggle="dropdown" aria-expanded="false">
            Contatos
            @if($contatosNaoLidos >= 1)
            <span class="label label-success ms-1">{{ $contatosNaoLidos }}</span>
            @endif
            <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuContatos">
            <li>
                <a href="{{ route('painel.contatos.index') }}" class="dropdown-item @if(Tools::routeIs('painel.contatos.index')) active @endif">Informações de contato</a>
            </li>
            <li>
                <a href="{{ route('painel.assuntos.index') }}" class="dropdown-item @if(Tools::routeIs('painel.assuntos*')) active @endif">Assuntos | E-mails</a>
            </li>
            <li>
                <a href="{{ route('painel.contatos-recebidos.index') }}" class="dropdown-item @if(Tools::routeIs('painel.contatos-recebidos*')) active @endif d-flex align-items-center">
                    Contatos recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success ms-1">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ route('painel.politica-de-privacidade.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.politica-de-privacidade*')) active @endif">Política de Privacidade</a>
    </li>

</ul>