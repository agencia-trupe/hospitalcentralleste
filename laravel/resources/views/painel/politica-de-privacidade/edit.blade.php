@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">POLÍTICA DE PRIVACIDADE</h2>
</legend>

{!! Form::model($politica, [
'route' => ['painel.politica-de-privacidade.update', $politica->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.politica-de-privacidade.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection