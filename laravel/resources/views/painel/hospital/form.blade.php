@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('quem_somos', 'QUEM SOMOS - Texto') !!}
    {!! Form::textarea('quem_somos', null, ['class' => 'form-control editor-basic']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('missao', 'MISSÃO - Texto') !!}
    {!! Form::textarea('missao', null, ['class' => 'form-control editor-clean']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('visao', 'VISÃO - Texto') !!}
    {!! Form::textarea('visao', null, ['class' => 'form-control editor-clean']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('valores', 'VALORES - Texto') !!}
    {!! Form::textarea('valores', null, ['class' => 'form-control editor-clean']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('nossa_historia', 'NOSSA HISTÓRIA - Texto') !!}
    {!! Form::textarea('nossa_historia', null, ['class' => 'form-control editor-basic']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('nossa_estrutura', 'NOSSA ESTRUTURA - Texto') !!}
    {!! Form::textarea('nossa_estrutura', null, ['class' => 'form-control editor-basic']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('qualidade_e_segurança', 'QUALIDADE E SEGURANÇA - Texto') !!}
    {!! Form::textarea('qualidade_e_segurança', null, ['class' => 'form-control editor-full']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}
</div>