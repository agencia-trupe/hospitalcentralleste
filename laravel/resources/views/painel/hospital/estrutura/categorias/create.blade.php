@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>NOSSA ESTRUTURA | Categorias | Imagens |</small> Adicionar Categoria</h2>
</legend>

{!! Form::open(['route' => 'painel.estrutura.categorias.store', 'files' => true]) !!}

@include('painel.hospital.estrutura.categorias.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection