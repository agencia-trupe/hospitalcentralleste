@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
<h2 class="m-0"><small>NOSSA ESTRUTURA | Categorias | Imagens |</small> Editar Categoria</h2>
</legend>

{!! Form::model($categoria, [
'route' => ['painel.estrutura.categorias.update', $categoria->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.hospital.estrutura.categorias.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection