@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>NOSSA ESTRUTURA | Destaques |</small> Adicionar Destaque</h2>
</legend>

{!! Form::open(['route' => 'painel.estrutura.destaques.store', 'files' => true]) !!}

@include('painel.hospital.estrutura.destaques.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection