@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0"><small>NOSSA HISTÓRIA |</small> Histórico</h2>

    <a href="{{ route('painel.historico.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Item
    </a>
</legend>


@if(!count($historico))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
@foreach($fases as $key => $fase)
<h4 class="mt-5 mb-0">{{ $fase }}</h4>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="historico">
        <thead>
            <tr>
                <th scope="col">Ano</th>
                <th scope="col">Imagem</th>
                <th scope="col">Frase</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($historico->where('fase', $key) as $item)
            <tr id="{{ $item->id }}">
                <td>{{ $item->ano }}</td>
                <td>
                    <img src="{{ asset('assets/img/historico/'.$item->imagem) }}" style="width: auto; max-width:100px;" alt="">
                </td>
                <td>{{ $item->frase }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.historico.destroy', $item->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.historico.edit', $item->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endforeach
@endif

@endsection