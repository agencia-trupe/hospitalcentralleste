@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>NOSSA HISTÓRIA |</small> Editar Item</h2>
</legend>

{!! Form::model($historico, [
'route' => ['painel.historico.update', $historico->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.hospital.historico.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection