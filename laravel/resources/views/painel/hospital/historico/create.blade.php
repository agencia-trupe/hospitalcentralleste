@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>NOSSA HISTÓRIA |</small> Adicionar Item</h2>
</legend>

{!! Form::open(['route' => 'painel.historico.store', 'files' => true]) !!}

@include('painel.hospital.historico.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection