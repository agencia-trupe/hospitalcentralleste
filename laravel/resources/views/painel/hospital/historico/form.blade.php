@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('fase', 'Fase') !!}
    {!! Form::select('fase', $fases , old('fase'), ['class' => 'form-control select-tipos']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('ano', 'Ano') !!}
    {!! Form::number('ano', null, ['class' => 'form-control input-ano']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    @if($historico->imagem)
    <img src="{{ url('assets/img/historico/'.$historico->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('frase', 'Frase') !!}
    {!! Form::text('frase', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.historico.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>