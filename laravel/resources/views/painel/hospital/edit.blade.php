@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">O HOSPITAL | Textos</h2>
</legend>

{!! Form::model($hospital, [
'route' => ['painel.hospital.update', $hospital->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.hospital.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection