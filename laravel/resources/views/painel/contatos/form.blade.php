@include('painel.layout.flash')

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('telefone', 'Telefone') !!}
        {!! Form::text('telefone', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('whatsapp', 'Whatsapp') !!}
        {!! Form::text('whatsapp', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('instagram', 'Instagram (link)') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_contatos', 'Texto Contatos') !!}
    {!! Form::textarea('texto_contatos', null, ['class' => 'form-control editor-basic']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_localizacao', 'Texto Localização') !!}
    {!! Form::textarea('texto_localizacao', null, ['class' => 'form-control editor-basic']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}
</div>