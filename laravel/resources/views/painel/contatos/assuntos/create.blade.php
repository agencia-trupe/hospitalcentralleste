@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CONTATOS | ASSUNTOS E E-MAILS |</small> Adicionar Assunto</h2>
</legend>

{!! Form::open(['route' => 'painel.assuntos.store', 'files' => true]) !!}

@include('painel.contatos.assuntos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection