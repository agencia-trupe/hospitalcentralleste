@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('titulo', 'Título do Assunto') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('email', 'E-mail de Recebimento') !!}
    {!! Form::text('email', null, ['class' => 'form-control input-text']) !!}
</div>
<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.assuntos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>