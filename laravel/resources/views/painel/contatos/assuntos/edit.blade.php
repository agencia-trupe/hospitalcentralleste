@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CONTATOS | ASSUNTOS E E-MAILS |</small> Editar Assunto</h2>
</legend>

{!! Form::model($assunto, [
'route' => ['painel.assuntos.update', $assunto->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.contatos.assuntos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection