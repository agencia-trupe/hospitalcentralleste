import Clipboard from "./modules/Clipboard.js";
import DataTables from "./modules/DataTables.js";
import DatePicker from "./modules/DatePicker.js";
import DeleteButton from "./modules/DeleteButton.js";
import ImagesUpload from "./modules/ImagesUpload.js";
import OrderImages from "./modules/OrderImages.js";
import OrderTable from "./modules/OrderTable.js";
import TextEditor from "./modules/TextEditor.js";
import MobileToggle from "../js/MobileToggle";

Clipboard();
DataTables();
DatePicker();
DeleteButton();
ImagesUpload();
OrderImages();
OrderTable();
TextEditor();
MobileToggle();

$(document).ready(function () {
    $(".input-ano").mask("0000");

    // SERVIÇOS - checkbox header
    $(".conteudo-header").css("display", "none");
    $(".checkbox-header").change(function () {
        $(this).toggleClass("checked");
        if ($(".checkbox-header").hasClass("checked")) {
            $(".conteudo-header").css("display", "block");
            $(".conteudo-header input").attr("required", "");
        } else {
            $(".conteudo-header").css("display", "none");
            $(".conteudo-header input").removeAttr("required");
        }
    });
    if ($(".checkbox-header").hasClass("checked")) {
        $(".conteudo-header").css("display", "block");
        $(".conteudo-header input").attr("required", "");
    } else {
        $(".conteudo-header").css("display", "none");
        $(".conteudo-header input").removeAttr("required");
    }

    // SERVIÇOS - checkbox home
    $(".conteudo-home").css("display", "none");
    $(".checkbox-home").change(function () {
        $(this).toggleClass("checked");
        if ($(".checkbox-home").hasClass("checked")) {
            $(".conteudo-home").css("display", "block");
            $(".conteudo-home input").children().attr("required", "");
        } else {
            $(".conteudo-home").css("display", "none");
            $(".conteudo-home input").children().removeAttr("required");
        }
    });
    if ($(".checkbox-home").hasClass("checked")) {
        $(".conteudo-home").css("display", "block");
        $(".conteudo-home input").children().attr("required", "");
    } else {
        $(".conteudo-home").css("display", "none");
        $(".conteudo-home input").children().removeAttr("required");
    }

    // SERVIÇOS - checkbox localização
    $(".conteudo-localizacao").css("display", "none");
    $(".checkbox-localizacao").change(function () {
        $(this).toggleClass("checked");
        if ($(".checkbox-localizacao").hasClass("checked")) {
            $(".conteudo-localizacao").css("display", "block");
            $(".conteudo-localizacao").children().attr("required", "");
        } else {
            $(".conteudo-localizacao").css("display", "none");
            $(".conteudo-localizacao").children().removeAttr("required");
        }
    });
    if ($(".checkbox-localizacao").hasClass("checked")) {
        $(".conteudo-localizacao").css("display", "block");
        $(".conteudo-localizacao").children().attr("required", "");
    } else {
        $(".conteudo-localizacao").css("display", "none");
        $(".conteudo-localizacao").children().removeAttr("required");
    }
});
