import MobileToggle from "./MobileToggle";

MobileToggle();

$(window).on("load", function () {});

$(document).ready(function () {
    // var urlPrevia = "";
    // var urlPrevia = "/previa-hospitalcentralleste";

    //Instafeed
    

    // HEADER - submenu O HOSPITAL
    if ($(window).width() < 800) {
        $("#navSubHospital, #navSubServicos").click(function (e) {
            e.preventDefault();
            $(".submenu-nav").css("display", "none");
            $("header .link-nav.sub-active").removeClass("sub-active");
            var submenu = $(this).attr("submenu");
            $(this)
                .parent()
                .children("." + submenu)
                .css("display", "flex");
            $(this).addClass("sub-active");
        });
    } else {
        $("#navSubHospital, #navSubServicos").mouseenter(function () {
            $(".submenu-nav").css("display", "none");
            $("header .link-nav.sub-active").removeClass("sub-active");
            var submenu = $(this).attr("submenu");
            $(this)
                .parent()
                .children("." + submenu)
                .css("display", "flex");
            $(this).addClass("sub-active");
        });
        $(".submenu-hospital, .submenu-servicos").mouseleave(function () {
            $(this).css("display", "none");
            $("header .link-nav.sub-active").removeClass("sub-active");
        });
    }

    // HOME - banners
    $(".banners")
        .on("cycle-initialized", function (e, o) {
            $(".banners").css("display", "block");
        })
        .cycle({
            slides: ".banner",
            fx: "fade",
            speed: 800,
            timeout: 4000,
        });

    // HOME - chamadas (margin-right)
    $("main.home article.chamadas .chamada:nth-child(3n)").css(
        "margin-right",
        "0"
    );

    // O HOSPITAL + SERVIÇOS - hover submenu
    $("main.o-hospital .link-submenu, main.servicos .link-submenu")
        .mouseenter(function () {
            $(this).addClass("hover");
        })
        .mouseleave(function () {
            $(this).removeClass("hover");
        });

    // O HOSPITAL - quem-somos-imagens + historico/img + destaques-imagens + estrutura-imagens
    $(".fancybox").fancybox({
        padding: 0,
        prevEffect: "fade",
        nextEffect: "fade",
        closeBtn: false,
        openEffect: "elastic",
        openSpeed: "150",
        closeEffect: "elastic",
        closeSpeed: "150",
        helpers: {
            title: {
                type: "outside",
                position: "top",
            },
            overlay: {
                css: {
                    background: "rgba(132, 134, 136, .88)",
                },
            },
        },
        fitToView: false,
        autoSize: false,
        beforeShow: function () {
            this.maxWidth = "90%";
            this.maxHeight = "90%";
        },
    });

    // O HOSPITAL - quem somos imagens - margin
    $("main.o-hospital .link-img-quem-somos:nth-child(3n)").css(
        "margin-right",
        "0"
    );

    // O HOSPITAL - histórico
    var alturaLinha = $("main.o-hospital .historico .center").height();
    if ($(window).width() < 1200) {
        console.log(alturaLinha - 100);
        $("main.o-hospital .historico .center .linha-historico").height(
            alturaLinha - 100
        );
    } else {
        $("main.o-hospital .historico .center .linha-historico").height(
            alturaLinha
        );
    }

    // O HOSPITAL - nossa estrutura / destaques / variações
    $("body").on(
        "click",
        "main.o-hospital .estrutura-destaques .link-variacao",
        function (e) {
            e.preventDefault();
            var linkAtivo = $(this).attr("href");
            var urlFancybox = $(this).attr("urlFancybox");
            $(
                "main.o-hospital .estrutura-destaques .link-img-destaque-capa"
            ).attr("href", urlFancybox);
            $("main.o-hospital .estrutura-destaques .img-destaque-capa").attr(
                "src",
                linkAtivo
            );
        }
    );

    // O HOSPITAL - nossa estrutura / imagens (AJAX)
    $(".link-filtro-categoria").first().addClass("active");
    $("body").on("click", ".link-filtro-categoria", function (e) {
        e.preventDefault();
        $(".link-filtro-categoria.active").removeClass("active");
        $(this).addClass("active");

        var url = $(this).attr("href");

        $("main.o-hospital .estrutura-imagens .imagens").animate(
            { opacity: "0.0" },
            800,
            function () {
                $.ajax({
                    type: "GET",
                    url: url,
                    cache: false,
                    beforeSend: function () {
                        $("main.o-hospital .estrutura-imagens .imagens").html(
                            ""
                        );
                    },
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);

                        data.imagens.forEach((imagem) => {
                            var html =
                                '<a href="' +
                                window.location.origin +
                                // urlPrevia +
                                "/assets/img/estrutura/imagens/big/" +
                                imagem.imagem +
                                '" class="link-imagem fancybox" rel="imagens"><img src="' +
                                window.location.origin +
                                // urlPrevia +
                                "/assets/img/estrutura/imagens/small/" +
                                imagem.imagem +
                                '" class="img-estrutura" alt=""></a>';
                            $(
                                "main.o-hospital .estrutura-imagens .imagens"
                            ).append(html);
                        });

                        $(
                            "main.o-hospital .estrutura-imagens .imagens"
                        ).animate({ opacity: "1.0" }, 800);

                        $(".fancybox").fancybox({
                            padding: 0,
                            prevEffect: "fade",
                            nextEffect: "fade",
                            closeBtn: false,
                            openEffect: "elastic",
                            openSpeed: "150",
                            closeEffect: "elastic",
                            closeSpeed: "150",
                            helpers: {
                                title: {
                                    type: "outside",
                                    position: "top",
                                },
                                overlay: {
                                    css: {
                                        background: "rgba(132, 134, 136, .88)",
                                    },
                                },
                            },
                            fitToView: false,
                            autoSize: false,
                            beforeShow: function () {
                                this.maxWidth = "90%";
                                this.maxHeight = "90%";
                            },
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    },
                });
            }
        );
    });

    // SERVICOS - destaques - grid
    $(".masonry-grid").masonryGrid({
        columns: 3,
    });

    // MASK telefone
    $(".input-telefone").mask("(00) 000000000");

    // AVISO DE COOKIES
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
        var url = window.location.origin + "/aceite-de-cookies";

        $.ajax({
            type: "POST",
            url: url,
            success: function (data, textStatus, jqXHR) {
                $(".aviso-cookies").hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
        });
    });
});
