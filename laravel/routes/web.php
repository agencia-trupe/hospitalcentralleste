<?php

use App\Http\Controllers\ContatoController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InstagramAuthController;
use App\Http\Controllers\LocalizacaoController;
use App\Http\Controllers\NovidadesController;
use App\Http\Controllers\OHospitalController;
use App\Http\Controllers\PoliticaDePrivacidadeController;
use App\Http\Controllers\ServicosController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('o-hospital/{submenu}', [OHospitalController::class, 'index'])->name('o-hospital');
Route::get('o-hospital/nossa-estrutura/imagens/{categoria}', [OHospitalController::class, 'getImagensCategoria'])->name('o-hospital.getImagensCategoria');
Route::get('servicos/{slug}', [ServicosController::class, 'index'])->name('servicos');
Route::get('novidades', [NovidadesController::class, 'index'])->name('novidades');
Route::get('localizacao', [LocalizacaoController::class, 'index'])->name('localizacao');
Route::get('contato', [ContatoController::class, 'index'])->name('contato');
Route::post('contato', [ContatoController::class, 'postContato'])->name('contato.post');
Route::get('politica-de-privacidade', [PoliticaDePrivacidadeController::class, 'index'])->name('politica-de-privacidade');
Route::post('aceite-de-cookies', [HomeController::class, 'postCookies'])->name('aceite-de-cookies.post');

require __DIR__ . '/auth.php';
