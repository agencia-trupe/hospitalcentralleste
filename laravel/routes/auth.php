<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Painel\AceiteDeCookiesController;
use App\Http\Controllers\Painel\AssuntosController;
use App\Http\Controllers\Painel\BannersController;
use App\Http\Controllers\Painel\ChamadasController;
use App\Http\Controllers\Painel\ConfiguracoesController;
use App\Http\Controllers\Painel\ContatosController;
use App\Http\Controllers\Painel\ContatosRecebidosController;
use App\Http\Controllers\Painel\EstruturaCategoriasController;
use App\Http\Controllers\Painel\EstruturaDestaquesController;
use App\Http\Controllers\Painel\EstruturaDestaquesImagensController;
use App\Http\Controllers\Painel\EstruturaImagensController;
use App\Http\Controllers\Painel\HistoricoController;
use App\Http\Controllers\Painel\HomeController;
use App\Http\Controllers\Painel\HospitalController;
use App\Http\Controllers\Painel\LocalizacoesController;
use App\Http\Controllers\Painel\MaisServicosController;
use App\Http\Controllers\Painel\PainelController;
use App\Http\Controllers\Painel\PoliticaDePrivacidadeController;
use App\Http\Controllers\Painel\QuemSomosImagensController;
use App\Http\Controllers\Painel\ServicosController;
use App\Http\Controllers\Painel\ServicosDestaquesController;
use App\Http\Controllers\Painel\UsersController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::prefix('painel')->group(function () {
    // AUTH - BREEZE
    Route::get('/login', [AuthenticatedSessionController::class, 'create'])
        ->middleware('guest')
        ->name('login');
    Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->middleware('guest');
    Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
        ->middleware('guest')
        ->name('password.request');
    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
        ->middleware('guest')
        ->name('password.email');
    Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
        ->middleware('guest')
        ->name('password.reset');
    Route::post('/reset-password', [NewPasswordController::class, 'store'])
        ->middleware('guest')
        ->name('password.update');
    Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->middleware('auth')
        ->name('password.confirm');
    Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
        ->middleware('auth');
    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->middleware('auth')
        ->name('logout');

    Route::middleware(['auth'])->group(function () {
        // Limpar caches
        Route::get('clear-cache', function () {
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('cache:clear');

            return 'DONE';
        });
        Route::get('/', [PainelController::class, 'index'])->name('painel');
        Route::post('image-upload', [PainelController::class, 'imageUpload']);
        Route::post('order', [PainelController::class, 'order']);
        Route::name('painel.')->group(function () {
            Route::resource('usuarios', UsersController::class)->except('show');
            Route::resource('configuracoes', ConfiguracoesController::class)->only(['index', 'update']);
            Route::resource('politica-de-privacidade', PoliticaDePrivacidadeController::class)->only(['index', 'update']);
            Route::get('aceite-de-cookies', [AceiteDeCookiesController::class, 'index'])->name('aceite-de-cookies.index');
            Route::resource('contatos', ContatosController::class)->only(['index', 'update']);
            Route::resource('localizacoes', LocalizacoesController::class)->except('show');
            Route::resource('assuntos', AssuntosController::class)->except('show');
            Route::get('contatos-recebidos/{id}/toggle', [ContatosRecebidosController::class, 'toggle'])->name('contatos-recebidos.toggle');
            Route::resource('contatos-recebidos', ContatosRecebidosController::class)->only(['index', 'show', 'destroy']);
            Route::resource('banners', BannersController::class)->except('show');
            Route::resource('home', HomeController::class)->only(['index', 'update']);
            Route::resource('chamadas', ChamadasController::class)->except('show');
            Route::resource('mais-servicos', MaisServicosController::class)->except('show');
            Route::resource('o-hospital', HospitalController::class)->only(['index', 'update'])->names('hospital');
            Route::get('o-hospital/quem-somos/imagens/clear', [QuemSomosImagensController::class, 'clear'])->name('quem-somos.imagens.clear');
            Route::resource('o-hospital/quem-somos/imagens', QuemSomosImagensController::class)->except('show')->names('quem-somos.imagens');
            Route::resource('o-hospital/nossa-historia/historico', HistoricoController::class)->except('show')->names('historico');
            Route::resource('o-hospital/nossa-estrutura/destaques', EstruturaDestaquesController::class)->except('show')->names('estrutura.destaques');
            Route::get('o-hospital/nossa-estrutura/destaques/{destaque}/imagens/clear', [EstruturaDestaquesImagensController::class, 'clear'])->name('estrutura.destaques.imagens.clear');
            Route::resource('o-hospital/nossa-estrutura/destaques.imagens', EstruturaDestaquesImagensController::class)->except('show')->names('estrutura.destaques.imagens');
            Route::resource('o-hospital/nossa-estrutura/categorias', EstruturaCategoriasController::class)->except('show')->names('estrutura.categorias');
            Route::get('o-hospital/nossa-estrutura/categorias/{categoria}/imagens/clear', [EstruturaImagensController::class, 'clear'])->name('estrutura.categorias.imagens.clear');
            Route::resource('o-hospital/nossa-estrutura/categorias.imagens', EstruturaImagensController::class)->except('show')->names('estrutura.categorias.imagens');
            Route::resource('servicos', ServicosController::class)->except('show');
            Route::resource('servicos.destaques', ServicosDestaquesController::class)->except('show');
        });
    });
});
