<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstruturaImagensTable extends Migration
{
    public function up()
    {
        Schema::create('estrutura_imagens', function (Blueprint $table) {
            $table->id();
            $table->foreignId('categoria_id')->constrained('estrutura_categorias')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('estrutura_imagens');
    }
}
