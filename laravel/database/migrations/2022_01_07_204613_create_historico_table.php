<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricoTable extends Migration
{
    public function up()
    {
        Schema::create('historico', function (Blueprint $table) {
            $table->id();
            $table->string('fase'); // fundacao, expansao, consolidacao 
            $table->integer('ano');
            $table->string('imagem')->nullable();
            $table->string('frase');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('historico');
    }
}
