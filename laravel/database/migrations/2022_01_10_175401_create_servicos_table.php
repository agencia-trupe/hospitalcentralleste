<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicosTable extends Migration
{
    public function up()
    {
        Schema::create('servicos', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->text('texto');
            $table->boolean('header')->default(false);
            $table->string('titulo_header')->nullable();
            $table->boolean('home')->default(false);
            $table->string('capa_home')->nullable();
            $table->string('titulo_home')->nullable();
            $table->boolean('localizacao')->default(false);
            $table->string('titulo_localizacao')->nullable();
            $table->foreignId('localizacao_id')->nullable()->constrained('localizacoes')->onDelete('cascade')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('servicos');
    }
}
