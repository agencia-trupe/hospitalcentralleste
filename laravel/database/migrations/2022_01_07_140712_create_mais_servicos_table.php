<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaisServicosTable extends Migration
{
    public function up()
    {
        Schema::create('mais_servicos', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mais_servicos');
    }
}
