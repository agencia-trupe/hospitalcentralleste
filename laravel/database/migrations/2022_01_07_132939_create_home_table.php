<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->id();
            $table->string('titulo_acessos');
            $table->string('frase_acessos');
            $table->string('titulo_chamadas');
            $table->string('frase_chamadas');
            $table->text('texto_chamadas');
            $table->string('titulo_novidades');
            $table->string('frase_novidades');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('home');
    }
}
