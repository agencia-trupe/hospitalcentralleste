<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalizacoesTable extends Migration
{
    public function up()
    {
        Schema::create('localizacoes', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('titulo_rodape');
            $table->string('endereco');
            $table->text('google_maps');
            $table->string('capa_home');
            $table->text('principais_acessos');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('localizacoes');
    }
}
