<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHospitalTable extends Migration
{
    public function up()
    {
        Schema::create('hospital', function (Blueprint $table) {
            $table->id();
            $table->text('quem_somos');
            $table->text('missao');
            $table->text('visao');
            $table->text('valores');
            $table->text('nossa_historia');
            $table->text('nossa_estrutura');
            $table->text('qualidade_e_segurança');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('hospital');
    }
}
