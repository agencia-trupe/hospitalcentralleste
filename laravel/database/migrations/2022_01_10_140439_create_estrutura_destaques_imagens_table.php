<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstruturaDestaquesImagensTable extends Migration
{
    public function up()
    {
        Schema::create('estrutura_destaques_imagens', function (Blueprint $table) {
            $table->id();
            $table->foreignId('destaque_id')->constrained('estrutura_destaques')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('estrutura_destaques_imagens');
    }
}
