<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstruturaDestaquesTable extends Migration
{
    public function up()
    {
        Schema::create('estrutura_destaques', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->text('texto');
            $table->string('video')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('estrutura_destaques');
    }
}
