<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(PoliticaDePrivacidadeTableSeeder::class);
        $this->call(ContatosTableSeeder::class);
        $this->call(LocalizacoesTableSeeder::class);
        $this->call(AssuntosTableSeeder::class);
        $this->call(HomeTableSeeder::class);
        $this->call(HospitalTableSeeder::class);
        $this->call(EstruturaCategoriasTableSeeder::class);
    }
}
