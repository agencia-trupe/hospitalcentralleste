<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocalizacoesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('localizacoes')->insert([
            'id'                 => 1,
            'ordem'              => 1,
            'titulo'             => 'CENTRO MÉDICO - PRONTO SOCORRO - MATERNIDADE - INTERNAÇÕES',
            'titulo_rodape'      => 'PRONTO SOCORRO GERAL ADULTO E INFANTIL',
            'endereco'           => 'Rua Cabo José Teixeira, 189 ∙ Vila Yolanda ∙ 08451-010 ∙ São Paulo, SP',
            'google_maps'        => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.8408563721036!2d-46.40569168440714!3d-23.538225666624868!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce6456f2fdb8c7%3A0x51ca4f8ed9fe04f4!2sRua%20Cabo%20Jos%C3%A9%20Teixeira%2C%20189%20-%20Vila%20Yolanda%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2008451-010!5e0!3m2!1spt-BR!2sbr!4v1641490897890!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>',
            'capa_home'          => '',
            'principais_acessos' => '',
        ]);

        DB::table('localizacoes')->insert([
            'id'                 => 2,
            'ordem'              => 2,
            'titulo'             => 'CENTRO DE DIAGNÓSTICOS',
            'titulo_rodape'      => 'CEMED - CENTRO MÉDICO DE ESPECIALIDADES E DIAGNÓSTICA | MATERNIDADE',
            'endereco'           => 'Rua Tingoassuiba, 50 ∙ Vila Yolanda ∙ 08451-030 ∙ São Paulo, SP',
            'google_maps'        => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.8350774971295!2d-46.40447698440715!3d-23.53843346663246!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce64566ec04727%3A0xa0d7bce767583008!2sR.%20Tingoassuiba%2C%2050%20-%20Vila%20Yolanda%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2008451-030!5e0!3m2!1spt-BR!2sbr!4v1641491073300!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>',
            'capa_home'          => '',
            'principais_acessos' => '',
        ]);
    }
}
