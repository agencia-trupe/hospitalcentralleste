<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AssuntosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assuntos')->insert([
            'id'     => 1,
            'ordem'  => 1,
            'titulo' => 'Dúvidas',
            'email'  => 'contato@trupe.net',
        ]);

        DB::table('assuntos')->insert([
            'id'     => 2,
            'ordem'  => 2,
            'titulo' => 'Exames',
            'email'  => 'contato@trupe.net',
        ]);

        DB::table('assuntos')->insert([
            'id'     => 3,
            'ordem'  => 3,
            'titulo' => 'Maternidade',
            'email'  => 'contato@trupe.net',
        ]);

        DB::table('assuntos')->insert([
            'id'     => 4,
            'ordem'  => 4,
            'titulo' => 'Sugestões',
            'email'  => 'contato@trupe.net',
        ]);

        DB::table('assuntos')->insert([
            'id'     => 5,
            'ordem'  => 5,
            'titulo' => 'Reclamações',
            'email'  => 'contato@trupe.net',
        ]);
    }
}
