<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HomeTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'titulo_acessos'   => 'NOSSOS PRINCIPAIS ACESSOS',
            'frase_acessos'    => 'Confira o endereço das diferentes entradas do complexo hospitalar',
            'titulo_chamadas'  => 'MAIS CONFIANÇA:',
            'frase_chamadas'   => 'INVESTIMENTOS SEGUEM METAS INTERNACIONAIS DE QUALIDADE E SEGURANÇA DO PACIENTE',
            'texto_chamadas'   => '',
            'titulo_novidades' => 'NOVIDADES',
            'frase_novidades'  => 'Acompanhe as novidades em nossas redes sociais',
        ]);
    }
}
