<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstruturaCategoriasTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('estrutura_categorias')->insert([
            'id'     => 1,
            'ordem'  => 1,
            'titulo' => 'recepção',
        ]);

        DB::table('estrutura_categorias')->insert([
            'id'     => 2,
            'ordem'  => 2,
            'titulo' => 'enfermaria',
        ]);

        DB::table('estrutura_categorias')->insert([
            'id'     => 3,
            'ordem'  => 3,
            'titulo' => 'internações (quartos)',
        ]);

        DB::table('estrutura_categorias')->insert([
            'id'     => 4,
            'ordem'  => 4,
            'titulo' => 'pronto socorro adulto',
        ]);

        DB::table('estrutura_categorias')->insert([
            'id'     => 5,
            'ordem'  => 5,
            'titulo' => 'pronto socorro pediátrico',
        ]);

        DB::table('estrutura_categorias')->insert([
            'id'     => 6,
            'ordem'  => 6,
            'titulo' => 'pronto socorro ortopédico',
        ]);

        DB::table('estrutura_categorias')->insert([
            'id'     => 7,
            'ordem'  => 7,
            'titulo' => 'maternidade',
        ]);

        DB::table('estrutura_categorias')->insert([
            'id'     => 8,
            'ordem'  => 8,
            'titulo' => 'centro cirúrgico',
        ]);

        DB::table('estrutura_categorias')->insert([
            'id'     => 9,
            'ordem'  => 9,
            'titulo' => 'laboratórios',
        ]);

        DB::table('estrutura_categorias')->insert([
            'id'     => 10,
            'ordem'  => 10,
            'titulo' => 'auditório e salas de apoio',
        ]);
    }
}
