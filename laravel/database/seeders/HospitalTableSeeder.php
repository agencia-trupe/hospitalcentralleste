<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HospitalTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('hospital')->insert([
            'quem_somos'            => '',
            'missao'                => '',
            'visao'                 => '',
            'valores'               => '',
            'nossa_historia'        => '',
            'nossa_estrutura'       => '',
            'qualidade_e_segurança' => '',
        ]);
    }
}
