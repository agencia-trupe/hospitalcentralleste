<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contatos')->insert([
            'telefone'          => '+55 11 2551 5151',
            'whatsapp'          => '',
            'instagram'         => 'https://www.instagram.com/hospitalcentralleste/',
            'texto_localizacao' => '',
            'texto_contatos'    => '',
        ]);
    }
}
