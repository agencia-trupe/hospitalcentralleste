const mix = require("laravel-mix");
// const webpack = require("webpack");
// const CKEditorWebpackPlugin = require("@ckeditor/ckeditor5-dev-webpack-plugin");
// const { styles } = require("@ckeditor/ckeditor5-dev-utils");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath("../public")
    .options({
        terser: { extractComments: false },
        processCssUrls: false,
    })
    .js("resources/assets/js/main.js", "assets/js")
    .js("resources/assets/painel/painel.js", "assets/js")
    // .webpackConfig({
    //     module: {
    //         rules: [
    //             {
    //                 test: /ckeditor5-[^/]+\/theme\/icons\/[^/]+\.svg$/,

    //                 use: ["raw-loader"],
    //             },
    //         ],
    //     },
    // })
    .stylus("resources/assets/stylus/main.styl", "assets/css", {
        stylusOptions: {
            use: [require("rupture")()],
        },
    })
    .stylus("resources/assets/painel/painel.styl", "assets/css", {
        stylusOptions: {
            use: [require("rupture")()],
        },
    })
    .browserSync("localhost:8000");

mix.inProduction() && mix.version();

// module.exports = {
//     plugins: [
//         // ...

//         new CKEditorWebpackPlugin({
//             // See https://ckeditor.com/docs/ckeditor5/latest/features/ui-language.html
//             language: "pl",
//         }),
//     ],

//     module: {
//         rules: [
//             {
//                 test: /ckeditor5-[^/\\]+[/\\]theme[/\\]icons[/\\][^/\\]+\.svg$/,
//                 use: ["raw-loader"],
//             },
//             {
//                 test: /ckeditor5-[^/\\]+[/\\]theme[/\\].+\.css$/,
//                 use: [
//                     {
//                         loader: "style-loader",
//                         options: {
//                             injectType: "singletonStyleTag",
//                             attributes: {
//                                 "data-cke": true,
//                             },
//                         },
//                     },
//                     {
//                         loader: "postcss-loader",
//                         options: styles.getPostCssConfig({
//                             themeImporter: {
//                                 themePath: require.resolve(
//                                     "@ckeditor/ckeditor5-theme-lark"
//                                 ),
//                             },
//                             minify: true,
//                         }),
//                     },
//                 ],
//             },
//         ],
//     },
// };
