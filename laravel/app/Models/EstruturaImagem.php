<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstruturaImagem extends Model
{
    use HasFactory;

    protected $table = 'estrutura_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $id)
    {
        return $query->where('categoria_id', $id);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 290,
                'height' => 220,
                'path'    => 'assets/img/estrutura/imagens/small/'
            ],
            [
                'width'  => 1500,
                'height' => null,
                'upsize'  => true,
                'path'    => 'assets/img/estrutura/imagens/big/'
            ]
        ]);
    }
}
