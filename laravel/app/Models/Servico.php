<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;

class Servico extends Model
{
    use HasFactory;
    use Sluggable;

    protected $table = 'servicos';

    protected $guarded = ['id'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'titulo'
            ]
        ];
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeHeader($query)
    {
        return $query->where('header', '=', 1);
    }

    public function scopeHome($query)
    {
        return $query->where('home', '=', 1);
    }

    public function scopeLocalizacao($query)
    {
        return $query->where('localizacao', '=', 1);
    }

    public function destaques()
    {
        return $this->hasMany('App\Models\ServicoDestaque', 'servico_id')->ordenados();
    }

    public static function upload_imagem()
    {
        return CropImage::make('capa_home', [
            'width'  => 280,
            'height' => 180,
            'path'   => 'assets/img/servicos/'
        ]);
    }
}
