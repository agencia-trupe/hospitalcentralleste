<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    use HasFactory;

    protected $table = 'historico';

    protected $guarded = ['id'];

    public static function fases()
    {
        return $fases = [
            'fundacao'     => 'Fundação',
            'expansao'     => 'Expansão',
            'consolidacao' => 'Consolidação',
        ];
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 200,
                'height' => null,
                'path'    => 'assets/img/historico/'
            ],
            [
                'width'  => 1500,
                'height' => null,
                'upsize'  => true,
                'path'    => 'assets/img/historico/big/'
            ]
        ]);
    }
}
