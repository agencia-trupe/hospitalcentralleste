<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstruturaDestaqueImagem extends Model
{
    use HasFactory;

    protected $table = 'estrutura_destaques_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeDestaque($query, $id)
    {
        return $query->where('destaque_id', $id);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 500,
                'height' => 335,
                'path'    => 'assets/img/estrutura/destaques/'
            ],
            [
                'width'  => 250,
                'height' => 165,
                'path'    => 'assets/img/estrutura/destaques/small/'
            ],
            [
                'width'  => 1500,
                'height' => null,
                'upsize'  => true,
                'path'    => 'assets/img/estrutura/destaques/big/'
            ]
        ]);
    }
}
