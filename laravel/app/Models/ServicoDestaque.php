<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicoDestaque extends Model
{
    use HasFactory;

    protected $table = 'servicos_destaques';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeServico($query, $id)
    {
        return $query->where('servico_id', $id);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 370,
                'height' => 210,
                'path'   => 'assets/img/servicos/destaques/'
            ],
            [
                'width'  => 1500,
                'height' => null,
                'upsize'  => true,
                'path'    => 'assets/img/servicos/destaques/big/'
            ]

        ]);
    }
}
