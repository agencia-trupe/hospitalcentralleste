<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuemSomosImagem extends Model
{
    use HasFactory;

    protected $table = 'quem_somos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 400,
                'height' => 265,
                'path'    => 'assets/img/quem-somos/'
            ],
            [
                'width'  => 1500,
                'height' => null,
                'upsize'  => true,
                'path'    => 'assets/img/quem-somos/big/'
            ]
        ]);
    }
}
