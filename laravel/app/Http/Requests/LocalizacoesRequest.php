<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LocalizacoesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'             => 'required',
            'endereco'           => 'required',
            'google_maps'        => 'required',
            'capa_home'          => 'image',
            'principais_acessos' => 'required',
        ];
    }
}
