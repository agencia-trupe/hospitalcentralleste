<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo_acessos'   => 'required',
            'frase_acessos'    => 'required',
            'titulo_chamadas'  => 'required',
            'frase_chamadas'   => 'required',
            'texto_chamadas'   => 'required',
            'titulo_novidades' => 'required',
            'frase_novidades'  => 'required',
        ];
    }
}
