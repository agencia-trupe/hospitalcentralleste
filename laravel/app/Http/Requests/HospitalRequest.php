<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HospitalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quem_somos'            => 'required',
            'missao'                => 'required',
            'visao'                 => 'required',
            'valores'               => 'required',
            'nossa_historia'        => 'required',
            'nossa_estrutura'       => 'required',
            'qualidade_e_segurança' => 'required',
        ];
    }
}
