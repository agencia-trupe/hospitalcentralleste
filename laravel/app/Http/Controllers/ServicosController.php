<?php

namespace App\Http\Controllers;

use App\Models\Localizacao;
use App\Models\Servico;
use App\Models\ServicoDestaque;
use Illuminate\Http\Request;

class ServicosController extends Controller
{
    public function index($slug)
    {
        $servicos = Servico::ordenados()->get();
        $servico = $servicos->where('slug', $slug)->first();
        $destaques = ServicoDestaque::where('servico_id', $servico->id)->ordenados()->get();

        if($servico->localizacao == 1) {
            $localizacao = Localizacao::where('id', $servico->localizacao_id)->first();
        } else {
            $localizacao = null;
        }
        // dd($localizacao);

        return view('frontend.servicos', compact('servicos', 'servico', 'destaques', 'localizacao'));
    }
}
