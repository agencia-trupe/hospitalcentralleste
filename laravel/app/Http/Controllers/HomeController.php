<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\Banner;
use App\Models\Chamada;
use App\Models\Home;
use App\Models\Localizacao;
use App\Models\MaisServico;
use App\Models\Servico;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $home = Home::first();
        $banners = Banner::ordenados()->get();
        $servicosHome = Servico::home()->ordenados()->take(4)->get();
        $localizacoes = Localizacao::ordenados()->get();
        $chamadas = Chamada::ordenados()->get();
        $maisServicos = MaisServico::ordenados()->get();

        // $token = "IGQVJValdpUlVXWlpNQXVrUVpFc1dST3RSVHdIWWZAtdkx4dGZAmazRvX3plOGF2ZAUpsR3hxUmZAlZA0dkc3JJZA1dGM1lCdkNFeXh1SDR4TXlKajNudmhLY3BlYmNjQmZADZAk90RjUycU5BbW1qLVUwaWtWSAZDZD";
        // $token = "b9aa688fc6fbcd63a00fabb5ebed8251";
        // $user = file_get_contents("https://api.instagram.com/v1/users/self/media/recent/?access_token=$token");
        // $user = json_decode($user, true)['data'];
        // dd($user);

        return view('frontend.home', compact('home', 'banners', 'servicosHome', 'localizacoes', 'chamadas', 'maisServicos'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
