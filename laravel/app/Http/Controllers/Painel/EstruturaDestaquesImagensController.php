<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EstruturaDestaquesImagensRequest;
use App\Models\EstruturaDestaque;
use App\Models\EstruturaDestaqueImagem;
use Illuminate\Http\Request;

class EstruturaDestaquesImagensController extends Controller
{
    public function index(EstruturaDestaque $destaque)
    {
        $imagens = EstruturaDestaqueImagem::destaque($destaque->id)->ordenados()->get();

        return view('painel.hospital.estrutura.destaques.imagens.index', compact('destaque', 'imagens'));
    }

    public function show(EstruturaDestaque $destaque, EstruturaDestaqueImagem $imagem)
    {
        return $imagem;
    }

    public function store(EstruturaDestaquesImagensRequest $request, EstruturaDestaque $destaque)
    {
        try {
            $input = $request->all();
            $input['imagem'] = EstruturaDestaqueImagem::upload_imagem();
            $input['destaque_id'] = $destaque->id;

            $imagem = EstruturaDestaqueImagem::create($input);

            $view = view('painel.hospital.estrutura.destaques.imagens.imagem', compact('destaque', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(EstruturaDestaque $destaque, EstruturaDestaqueImagem $imagen)
    {
        try {
            $imagen->delete();

            return redirect()->route('painel.estrutura.destaques.imagens.index', $destaque->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear(EstruturaDestaque $destaque)
    {
        try {
            $destaque->imagens()->delete();

            return redirect()->route('painel.estrutura.destaques.imagens.index', $destaque->id)->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
