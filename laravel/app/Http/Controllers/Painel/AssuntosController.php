<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AssuntosRequest;
use App\Models\Assunto;
use Illuminate\Http\Request;

class AssuntosController extends Controller
{
    public function index()
    {
        $assuntos = Assunto::ordenados()->get();

        return view('painel.contatos.assuntos.index', compact('assuntos'));
    }

    public function create()
    {
        return view('painel.contatos.assuntos.create');
    }

    public function store(AssuntosRequest $request)
    {
        try {
            $input = $request->all();

            Assunto::create($input);

            return redirect()->route('painel.assuntos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Assunto $assunto)
    {
        return view('painel.contatos.assuntos.edit', compact('assunto'));
    }

    public function update(AssuntosRequest $request, Assunto $assunto)
    {
        try {
            $input = $request->all();

            $assunto->update($input);

            return redirect()->route('painel.assuntos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Assunto $assunto)
    {
        try {
            $assunto->delete();

            return redirect()->route('painel.assuntos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
