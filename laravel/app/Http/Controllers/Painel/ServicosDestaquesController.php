<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicosDestaquesRequest;
use App\Models\Servico;
use App\Models\ServicoDestaque;
use Illuminate\Http\Request;

class ServicosDestaquesController extends Controller
{
    public function index(Servico $servico)
    {
        $destaques = ServicoDestaque::servico($servico->id)->ordenados()->get();

        return view('painel.servicos.destaques.index', compact('servico', 'destaques'));
    }

    public function create(Servico $servico)
    {
        return view('painel.servicos.destaques.create', compact('servico'));
    }

    public function store(ServicosDestaquesRequest $request, Servico $servico)
    {
        try {
            $input = $request->all();
            $input['servico_id'] = $servico->id;
            if (isset($input['imagem'])) $input['imagem'] = ServicoDestaque::upload_imagem();

            ServicoDestaque::create($input);

            return redirect()->route('painel.servicos.destaques.index', $servico->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Servico $servico, ServicoDestaque $destaque)
    {
        return view('painel.servicos.destaques.edit', compact('servico', 'destaque'));
    }

    public function update(ServicosDestaquesRequest $request, Servico $servico, ServicoDestaque $destaque)
    {
        try {
            $input = $request->all();
            $input['servico_id'] = $servico->id;
            if (isset($input['imagem'])) $input['imagem'] = ServicoDestaque::upload_imagem();

            $destaque->update($input);

            return redirect()->route('painel.servicos.destaques.index', $servico->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Servico $servico, ServicoDestaque $destaque)
    {
        try {
            $destaque->delete();

            return redirect()->route('painel.servicos.destaques.index', $servico->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
