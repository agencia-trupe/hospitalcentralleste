<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\HistoricoRequest;
use App\Models\Historico;
use Illuminate\Http\Request;

class HistoricoController extends Controller
{
    public function index()
    {
        $historico = Historico::orderBy('ano', 'asc')->get();
        $fases = Historico::fases();

        return view('painel.hospital.historico.index', compact('historico', 'fases'));
    }

    public function create()
    {
        $fases = Historico::fases();

        return view('painel.hospital.historico.create', compact('fases'));
    }

    public function store(HistoricoRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Historico::upload_imagem();

            Historico::create($input);

            return redirect()->route('painel.historico.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Historico $historico)
    {
        $fases = Historico::fases();

        return view('painel.hospital.historico.edit', compact('historico', 'fases'));
    }

    public function update(HistoricoRequest $request, Historico $historico)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Historico::upload_imagem();
            
            $historico->update($input);

            return redirect()->route('painel.historico.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Historico $historico)
    {
        try {
            $historico->delete();

            return redirect()->route('painel.historico.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
