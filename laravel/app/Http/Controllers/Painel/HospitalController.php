<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\HospitalRequest;
use App\Models\Hospital;
use Illuminate\Http\Request;

class HospitalController extends Controller
{
    public function index()
    {
        $hospital = Hospital::first();

        return view('painel.hospital.edit', compact('hospital'));
    }

    public function update(HospitalRequest $request, Hospital $o_hospital)
    {
        try {
            $input = $request->all();

            $o_hospital->update($input);

            return redirect()->route('painel.hospital.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
