<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\MaisServicosRequest;
use App\Models\MaisServico;
use Illuminate\Http\Request;

class MaisServicosController extends Controller
{
    public function index()
    {
        $servicos = MaisServico::ordenados()->get();

        return view('painel.home.mais-servicos.index', compact('servicos'));
    }

    public function create()
    {
        return view('painel.home.mais-servicos.create');
    }

    public function store(MaisServicosRequest $request)
    {
        try {
            $input = $request->all();

            MaisServico::create($input);

            return redirect()->route('painel.mais-servicos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(MaisServico $mais_servico)
    {
        $servico = $mais_servico;
        return view('painel.home.mais-servicos.edit', compact('servico'));
    }

    public function update(MaisServicosRequest $request, MaisServico $mais_servico)
    {
        try {
            $input = $request->all();

            $mais_servico->update($input);

            return redirect()->route('painel.mais-servicos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(MaisServico $mais_servico)
    {
        try {
            $mais_servico->delete();

            return redirect()->route('painel.mais-servicos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
