<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EstruturaImagensRequest;
use App\Models\EstruturaCategoria;
use App\Models\EstruturaImagem;
use Illuminate\Http\Request;

class EstruturaImagensController extends Controller
{
    public function index(EstruturaCategoria $categoria)
    {
        $imagens = EstruturaImagem::categoria($categoria->id)->ordenados()->get();

        return view('painel.hospital.estrutura.categorias.imagens.index', compact('categoria', 'imagens'));
    }

    public function show(EstruturaCategoria $categoria, EstruturaImagem $imagem)
    {
        return $imagem;
    }

    public function store(EstruturaImagensRequest $request, EstruturaCategoria $categoria)
    {
        try {
            $input = $request->all();
            $input['imagem'] = EstruturaImagem::upload_imagem();
            $input['categoria_id'] = $categoria->id;

            $imagem = EstruturaImagem::create($input);

            $view = view('painel.hospital.estrutura.categorias.imagens.imagem', compact('categoria', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(EstruturaCategoria $categoria, EstruturaImagem $imagen)
    {
        try {
            $imagen->delete();

            return redirect()->route('painel.estrutura.categorias.imagens.index', $categoria->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear(EstruturaCategoria $categoria)
    {
        try {
            $categoria->imagens()->delete();

            return redirect()->route('painel.estrutura.categorias.imagens.index', $categoria->id)->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
