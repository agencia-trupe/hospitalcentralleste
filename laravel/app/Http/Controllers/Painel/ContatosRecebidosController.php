<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Assunto;
use App\Models\ContatoRecebido;
use Illuminate\Http\Request;

class ContatosRecebidosController extends Controller
{
    public function index()
    {
        $contatos = ContatoRecebido::join('assuntos', 'assuntos.id', '=', 'contatos_recebidos.assunto')
            ->select('assuntos.titulo as assunto_titulo', 'assuntos.email as assunto_email', 'contatos_recebidos.*')
            ->orderBy('created_at', 'DESC')->get();

        return view('painel.contatos.recebidos.index', compact('contatos'));
    }

    public function show(ContatoRecebido $contatos_recebido)
    {
        $contatos_recebido->update(['lido' => 1]);
        $recebido = $contatos_recebido;
        $assunto = Assunto::where('id', $recebido->assunto)->first();
        
        return view('painel.contatos.recebidos.show', compact('recebido', 'assunto'));
    }

    public function destroy(ContatoRecebido $contatos_recebido)
    {
        try {
            $contatos_recebido->delete();

            return redirect()->route('painel.contatos-recebidos.index')->with('success', 'Mensagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: ' . $e->getMessage()]);
        }
    }

    public function toggle($id)
    {
        try {
            $recebido = ContatoRecebido::where('id', $id)->first();
            $recebido->update([
                'lido' => !$recebido->lido
            ]);

            return redirect()->route('painel.contatos-recebidos.index')->with('success', 'Mensagem alterada com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: ' . $e->getMessage()]);
        }
    }
}
