<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuemSomosImagensRequest;
use App\Models\QuemSomosImagem;
use Illuminate\Http\Request;

class QuemSomosImagensController extends Controller
{
    public function index()
    {
        $imagens = QuemSomosImagem::ordenados()->get();

        return view('painel.hospital.quem-somos.index', compact('imagens'));
    }

    public function show(QuemSomosImagem $imagen)
    {
        $imagem = $imagen;
        return $imagem;
    }

    public function store(QuemSomosImagensRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = QuemSomosImagem::upload_imagem();

            $imagem = QuemSomosImagem::create($input);

            $view = view('painel.hospital.quem-somos.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(QuemSomosImagem $imagen)
    {
        try {
            $imagen->delete();

            return redirect()->route('painel.quem-somos.imagens.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear()
    {
        try {
            $imagens = QuemSomosImagem::ordenados()->get();
            QuemSomosImagem::whereIn('id', $imagens->pluck('id'))->delete();

            return redirect()->route('painel.quem-somos.imagens.index')->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
