<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChamadasRequest;
use App\Models\Chamada;
use Illuminate\Http\Request;

class ChamadasController extends Controller
{
    public function index()
    {
        $chamadas = Chamada::ordenados()->get();

        return view('painel.home.chamadas.index', compact('chamadas'));
    }

    public function create()
    {
        return view('painel.home.chamadas.create');
    }

    public function store(ChamadasRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Chamada::upload_imagem();

            Chamada::create($input);

            return redirect()->route('painel.chamadas.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Chamada $chamada)
    {
        return view('painel.home.chamadas.edit', compact('chamada'));
    }

    public function update(ChamadasRequest $request, Chamada $chamada)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Chamada::upload_imagem();

            $chamada->update($input);

            return redirect()->route('painel.chamadas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Chamada $chamada)
    {
        try {
            $chamada->delete();

            return redirect()->route('painel.chamadas.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
