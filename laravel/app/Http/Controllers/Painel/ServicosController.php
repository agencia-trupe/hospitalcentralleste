<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicosRequest;
use App\Models\Localizacao;
use App\Models\Servico;
use Illuminate\Http\Request;

class ServicosController extends Controller
{
    public function index()
    {
        $servicos = Servico::ordenados()->get();

        return view('painel.servicos.index', compact('servicos'));
    }

    public function create()
    {
        $localizacoes = Localizacao::ordenados()->pluck('titulo', 'id');

        return view('painel.servicos.create', compact('localizacoes'));
    }

    public function store(ServicosRequest $request)
    {
        try {
            $input = $request->all();

            if ($input['header'] == 0) $input['titulo_header'] = null;
            if ($input['home'] == 0) {
                $input['titulo_home'] = null;
                $input['capa_home'] = null;
            } else {
                if (isset($input['capa_home'])) $input['capa_home'] = Servico::upload_imagem();
            }
            if ($input['localizacao'] == 0) {
                $input['titulo_localizacao'] = null;
                $input['localizacao_id'] = null;
            }

            Servico::create($input);

            return redirect()->route('painel.servicos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Servico $servico)
    {
        $localizacoes = Localizacao::ordenados()->pluck('titulo', 'id');

        return view('painel.servicos.edit', compact('servico', 'localizacoes'));
    }

    public function update(ServicosRequest $request, Servico $servico)
    {
        try {
            $input = $request->all();

            if ($input['header'] == 0) $input['titulo_header'] = null;
            if ($input['home'] == 0) {
                $input['titulo_home'] = null;
                $input['capa_home'] = null;
            } else {
                if (isset($input['capa_home'])) $input['capa_home'] = Servico::upload_imagem();
            }
            if ($input['localizacao'] == 0) {
                $input['titulo_localizacao'] = null;
                $input['localizacao_id'] = null;
            }

            $servico->update($input);

            return redirect()->route('painel.servicos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Servico $servico)
    {
        try {
            $servico->delete();

            return redirect()->route('painel.servicos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
