<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\LocalizacoesRequest;
use App\Models\Localizacao;
use Illuminate\Http\Request;

class LocalizacoesController extends Controller
{
    public function index()
    {
        $localizacoes = Localizacao::ordenados()->get();

        return view('painel.localizacoes.index', compact('localizacoes'));
    }

    public function create()
    {
        return view('painel.localizacoes.create');
    }

    public function store(LocalizacoesRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['capa_home'])) $input['capa_home'] = Localizacao::upload_capa();

            $count = count(Localizacao::get());

            if($count < 3) {
                Localizacao::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Podem ser adicionadas até 3 localizações.']);
            }


            return redirect()->route('painel.localizacoes.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Localizacao $localizaco)
    {
        $localizacao = $localizaco;
        return view('painel.localizacoes.edit', compact('localizacao'));
    }

    public function update(LocalizacoesRequest $request, Localizacao $localizaco)
    {
        try {
            $input = $request->all();

            if (isset($input['capa_home'])) $input['capa_home'] = Localizacao::upload_capa();

            $localizaco->update($input);

            return redirect()->route('painel.localizacoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Localizacao $localizaco)
    {
        try {
            $localizaco->delete();

            return redirect()->route('painel.localizacoes.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
