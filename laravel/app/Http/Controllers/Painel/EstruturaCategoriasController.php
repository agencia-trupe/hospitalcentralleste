<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EstruturaCategoriasRequest;
use App\Models\EstruturaCategoria;
use Illuminate\Http\Request;

class EstruturaCategoriasController extends Controller
{
    public function index()
    {
        $categorias = EstruturaCategoria::ordenados()->get();

        return view('painel.hospital.estrutura.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.hospital.estrutura.categorias.create');
    }

    public function store(EstruturaCategoriasRequest $request)
    {
        try {
            $input = $request->all();

            EstruturaCategoria::create($input);

            return redirect()->route('painel.estrutura.categorias.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(EstruturaCategoria $categoria)
    {
        return view('painel.hospital.estrutura.categorias.edit', compact('categoria'));
    }

    public function update(EstruturaCategoriasRequest $request, EstruturaCategoria $categoria)
    {
        try {
            $input = $request->all();

            $categoria->update($input);

            return redirect()->route('painel.estrutura.categorias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(EstruturaCategoria $categoria)
    {
        try {
            $categoria->delete();

            return redirect()->route('painel.estrutura.categorias.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
