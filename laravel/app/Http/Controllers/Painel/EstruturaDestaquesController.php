<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EstruturaDestaquesRequest;
use App\Models\EstruturaDestaque;
use Illuminate\Http\Request;

class EstruturaDestaquesController extends Controller
{
    public function index()
    {
        $destaques = EstruturaDestaque::ordenados()->get();

        return view('painel.hospital.estrutura.destaques.index', compact('destaques'));
    }

    public function create()
    {
        return view('painel.hospital.estrutura.destaques.create');
    }

    public function store(EstruturaDestaquesRequest $request)
    {
        try {
            $input = $request->all();

            EstruturaDestaque::create($input);

            return redirect()->route('painel.estrutura.destaques.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(EstruturaDestaque $destaque)
    {
        return view('painel.hospital.estrutura.destaques.edit', compact('destaque'));
    }

    public function update(EstruturaDestaquesRequest $request, EstruturaDestaque $destaque)
    {
        try {
            $input = $request->all();

            $destaque->update($input);

            return redirect()->route('painel.estrutura.destaques.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(EstruturaDestaque $destaque)
    {
        try {
            $destaque->delete();

            return redirect()->route('painel.estrutura.destaques.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
