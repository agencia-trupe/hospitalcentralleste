<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NovidadesController extends Controller
{
    public function index()
    {
        return view('frontend.novidades');
    }
}
