<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Mail\ContatosRecebidos;
use App\Models\Assunto;
use App\Models\ContatoRecebido;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContatoController extends Controller
{
    public function index()
    {
        $assuntos = Assunto::ordenados()->pluck('titulo', 'id');

        return view('frontend.contato', compact('assuntos'));
    }

    public function postContato(ContatosRecebidosRequest $request)
    {
        try {
            $data = $request->all();

            $contato = ContatoRecebido::create($data);

            $assunto = Assunto::where('id', $data['assunto'])->first();
            $contato['assunto_titulo'] = $assunto->titulo;

            Mail::to($assunto->email)->send(new ContatosRecebidos($contato));

            return redirect()->back()->with('enviado', true);
        } catch (\Exception $e) {

            return back()->withErrors([$e->getMessage()]);
        }
    }
}
