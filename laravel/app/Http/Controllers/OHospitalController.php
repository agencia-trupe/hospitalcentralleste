<?php

namespace App\Http\Controllers;

use App\Models\EstruturaCategoria;
use App\Models\EstruturaDestaque;
use App\Models\EstruturaDestaqueImagem;
use App\Models\EstruturaImagem;
use App\Models\Historico;
use App\Models\Hospital;
use App\Models\QuemSomosImagem;
use Illuminate\Http\Request;

class OHospitalController extends Controller
{
    public function index($submenu)
    {
        $hospital = Hospital::first();

        if ($submenu == 'quem-somos') {
            $quemSomos = QuemSomosImagem::ordenados()->get();
            return view('frontend.o-hospital.quem-somos', compact('hospital', 'quemSomos'));
        }

        if ($submenu == 'nossa-historia') {
            $historicoFundacao = Historico::where('fase', 'fundacao')->orderBy('ano', 'asc')->get();
            $historicoExpansao = Historico::where('fase', 'expansao')->orderBy('ano', 'asc')->get();
            $historicoConsolidacao = Historico::where('fase', 'consolidacao')->orderBy('ano', 'asc')->get();
            return view('frontend.o-hospital.nossa-historia', compact('hospital', 'historicoFundacao', 'historicoExpansao', 'historicoConsolidacao'));
        }

        if ($submenu == 'nossa-estrutura') {
            $destaques = EstruturaDestaque::ordenados()->get();
            $destaquesImagens = EstruturaDestaqueImagem::ordenados()->get();
            $categorias = EstruturaCategoria::ordenados()->get();
            $imagensFirst = EstruturaImagem::where('categoria_id', $categorias->first()->id)->ordenados()->get();
            return view('frontend.o-hospital.nossa-estrutura', compact('hospital', 'destaques', 'destaquesImagens', 'categorias', 'imagensFirst'));
        }

        if ($submenu == 'qualidade-e-seguranca') {
            return view('frontend.o-hospital.qualidade-e-seguranca', compact('hospital'));
        }
    }

    public function getImagensCategoria($categoria)
    {
        $imagens = EstruturaImagem::where('categoria_id', $categoria)->ordenados()->get();

        return response()->json(['imagens' => $imagens]);
    }
}
