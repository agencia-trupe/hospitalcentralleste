<?php

namespace App\Mail;

use App\Models\ContatoRecebido;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContatosRecebidos extends Mailable
{
    use Queueable, SerializesModels;

    public $contato;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContatoRecebido $contatos_recebido)
    {
        $this->contato = $contatos_recebido;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[CONTATOS RECEBIDOS - ASSUNTO: ' . $this->contato['assunto_titulo'] . '] ' . config('app.name'))->view('emails.contato');
    }
}
