<?php

namespace App\Providers;

use App\Models\AceiteDeCookies;
use App\Models\Assunto;
use App\Models\Configuracao;
use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Models\Localizacao;
use App\Models\Servico;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $view->with('config', Configuracao::first());
        });

        View::composer('frontend.*', function ($view) {
            $view->with('contato', Contato::first());
            $view->with('localizacoes', Localizacao::ordenados()->get());

            $request = app(\Illuminate\Http\Request::class);
            $view->with('verificacao', AceiteDeCookies::where('ip', $request->ip())->first());
        });

        View::composer('frontend.layout.*', function ($view) {
            $view->with('servicosHeader', Servico::header()->ordenados()->take(4)->get());
            $view->with('servicos', Servico::ordenados()->get());
            $view->with('servicoFirst', Servico::ordenados()->first());
        });


        View::composer('painel.layout.*', function ($view) {
            $view->with('contatosNaoLidos', ContatoRecebido::naoLidos()->count());
        });
    }
}
